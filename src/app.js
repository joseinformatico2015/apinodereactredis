
const express= require('express');
const redis= require('redis');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
const axios = require('axios');
const config = require('../configs/config');
const cors = require('cors');
const fetch = require("node-fetch");
const path = require('path');

const app= express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

app.use(express.static(path.join(__dirname,"..","dist")))


app.set('master_key', config.llave);
const client = redis.createClient({
    host: 'redis-server',
    port: 6379
})

client.on('connect', ()=>{
    console.log('Readis is connected');
})

client.on('error', function(err) {
    console.log('Redis error: ' + err);
});

const authMiddleware = express.Router(); 
authMiddleware.use((req, res, next) => {
    const token = req.headers['access-token'];
    if (token) {
      jwt.verify(token, app.get('master_key'), (err, decoded) => {      
        if (err) {
          return res.json({ mensaje: 'Session expired, please login again' });    
        } else {
          req.decoded = decoded;    
          next();
        }
      });
    } else {
      res.send({ 
          mensaje: 'Token not Provided' 
      });
    }
 });

app.get("",(req,res)=>{
    res.sendFile(path.join(__dirname,"..","index.html"));
})

app.post('/user/add',(req, res)=>{
    let username=req.body.username;
    let password=req.body.password;
    let create_at=Date.now();
    client.hmset(username,["password", password,"create_at", create_at],function(err,resp){
        if(err){
            console.log(err);
            res.json({ message: "error" });
        }else{
            console.log("guardado");
            res.json({ message: "ok user created" });
        }
    })
})


app.post('/login',(req, res)=>{
    let username=req.body.username;
    let password=req.body.password;
    client.hgetall(username,function(err,resp){
        if(err){
            console.log(err);
            res.json({ message: "error" });
        }else{
            if(resp==null){
                res.json({ message: "The Username is incorrect" });
            }else{
                if (resp.password==password) {
                //     res.json({ message: "login correcto" });
                // } else {
                //     res.json({ message: "login incorrecto" });
                // }
                    const payload = {
                        check:  true
                    };
                    const token = jwt.sign(payload, app.get('master_key'), {
                        expiresIn:  120
                        // expiresIn: 60 * 60 * 24 // expires in 24 hours
                    });
                    res.json({
                        mensaje: 'Authenticated user',
                        token: token
                    });
                } else {
                    res.json({ mensaje: "The pass is incorrect"})
                }
            }
        }
    })
})

   
async function fetchChar() {
    let res = await fetch("https://rickandmortyapi.com/api/character/");
    let data = await res.json();
    console.log(data.info.pages)

    const allPages = await Promise.all(
        Array(data.info.pages - 1)
        .fill(0)
        .map((i,index) =>
            fetch(`https://rickandmortyapi.com/api/character/?page=${index + 2}`).then(res => res.json()).then(d => d.results)
        )
    )
    const fullData = allPages.reduce((acc, d) => [...acc, ...d], []);
    return [...data.results, ...fullData];

}
    

app.get('/data', authMiddleware, async (req, res) => {
    const datos =await fetchChar();
    // console.log(datos.filter((item)=>{return item.id==7;}));
     res.json(datos);
});


module.exports =  app;
import React, { useState, useEffect } from 'react'
import e from 'cors';
const axios = require('axios');


const Data=(props)=>{
    const [data, setData] = useState({ data: [] });
    const [message, setMessage] = useState("loading data...");

    const getData = async (token) => {
        var config = {
            headers: { 'access-token': token,'Content-Type': 'application/json' }
        };
        const data = await axios.get('/data',config)

        setData(data);
        console.log(JSON.stringify(data)+"aqui");

        console.log(data.mensaje+"este messege");

        
        if(data.mensaje){
            setMessage(JSON.stringify(data.data.mensaje));
            console.log(message+"con messege");
        }else{
            setMessage(JSON.stringify(data.data));
            console.log(message+"no messege");
        }
    };

    useEffect(() => {
        console.log(props.location.state)
        let token="";
        const token_storage=JSON.parse(localStorage.getItem('token'));
        // console.log(token);
        props.location.state?
            token=props.location.state.token
            :
            token=token_storage
        getData(token);
       
    },[]);



    return (
        <div>
        <h1>{message}</h1>
        </div>
    )
}

export default Data

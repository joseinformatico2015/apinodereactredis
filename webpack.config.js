const path=require('path');
const HtmlWebpackPlugin=require('html-webpack-plugin');
const {CleanWebpackPlugin}=require('clean-webpack-plugin');

module.exports={
    entry:["@babel/polyfill", path.resolve(__dirname, "src","index.js")],
    // entry: path.resolve(__dirname, "src","js","main.js"),
    output:{
        path: path.resolve(__dirname,"dist"),
        filename:"bundle.js"
    },
    // entry:["@babel/polyfill",'./src/index.js'],
    // output:{
    //     filename:'bundle.[hash].js',
    //     path: path.resolve(__dirname, 'dist'),
    //     publicPath: '/'
    // },
    mode:'development',
    // mode:'production',
    resolve:{
        extensions:['.js','.jsx']
    },
    module:{
        rules:[
            {
                // test: /\.jsx?/,
                // exclude: /node_modules/,
                // loader:[
                //     {
                //         loader:"babel-loader",
                //         query:{
                //             presets:["@babel/preset-react"]
                //         }
                //     }
                // ]
                test: /\.(js|jsx)$/,
                use: ['babel-loader'],
                exclude: /node_modules/
            }
        ]
    },
    // devServer:{
    //     historyApiFallback: true,
    //     port:4000
    // },



    plugins:[
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            template: './src/index.html'
        }),
    ]
}